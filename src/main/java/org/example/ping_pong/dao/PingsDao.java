package org.example.ping_pong.dao;

public interface PingsDao {
    int getPingsCount();

    /**
     * @return New (incremented) pings count.
     */
    int incrementPingsCount();
}
