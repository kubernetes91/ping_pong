package org.example.ping_pong.services;

public interface PingService {
    /**
     * @return New (incremented) pings count.
     */
    int incrementPingsCount();

    int getPingsCount();
}
