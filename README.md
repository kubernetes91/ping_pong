### Run in Idea

#### Postgres settings

The application stores data in a Postgres instance.
View required environment variables in the `local.env` file in the root folder.
These variables specify postgres configuration.

Run a Postgres server on URL specified in the configuration, and create 
required database, a user account - will be used by the ping_pong app,
and privileges for the user. 

```shell
create database ${POSTGRES_DB};
create user ${POSTGRES_USERNAME} with encrypted password '${POSTGRES_PASSWORD}';
grant all privileges on database ${POSTGRES_DB} to ${POSTGRES_USERNAME};
```
#### Run app

Run main application class [PingPongApplication](src/main/java/org/example/ping_pong/PingPongApplication.java).
Application expects environment variables specified in the `local.env` file in the root folder.

App is available on port 8080: http://localhost:8080/pingpong, http://localhost:8080/count

### Run in k3s Kubernetes cluster

Run following commands from the folder with manifests for k3s cluster.

#### Run a kubernetes cluster forwarding host port 8081 to a load-balancer port 80

Create cluster with default name
```shell
k3d cluster create -p 8081:80@loadbalancer --agents 2
```

#### Encrypt secret

Encrypt secret.yml use your own public key for that `sops --encrypt --age <public_key> secret.yml > secret.enc.yml`:
```shell
sops --encrypt --age age1ssqylrszr8sj6ys8glqhrr7w0zkdve80kyndcsuhu3qsx3zywaxsxph2z0 manifests/k3s/secret.yml > manifests/k3s/secret.enc.yml
```

You can verify encrypted secret:
```shell
export SOPS_AGE_KEY_FILE=<path to age key file>
sops --decrypt manifests/k3s/secret.enc.yml > manifests/k3s/secret.decrypted.yml
```
#### Run pin-pong app on the cluster

Create a namespace:
```shell
kubectl apply -f manifests/k3s/namespace.yml
```

Create secret:
```shell
kubectl apply -f manifests/k3s/secret.yml
```

Alternatively deploy decrypted secret:
```shell
export SOPS_AGE_KEY_FILE=<path to age key file>
sops --decrypt manifests/k3s/secret.enc.yml | kubectl apply -f -
```

Lunch Postgres:
```shell
kubectl apply -f manifests/k3s/postgres.yml
```

Deploy ping-pong:
```shell
kubectl apply -f manifests/k3s/deployment.yml -f manifests/k3s/service.yml -f manifests/k3s/ingress.yml
```

App is available on port 8081: http://localhost:8081/pingpong, http://localhost:8081/count

### Run in Google Kubernetes Engine

Run from `GKE-lb` manifests folder.
Secret encryption works the same way as in previous section, so view that there.
Create a namespace, wait until it would be ready, and deploy other services.

App is available on port 80, on paths: `/pingpong` and `/count`.
