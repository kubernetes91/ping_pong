package org.example.ping_pong.controllers;

import lombok.RequiredArgsConstructor;
import org.example.ping_pong.services.PingService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class PingController {

    private final PingService pingService;

    @ResponseBody
    @GetMapping("/pingpong")
    public String ping() {
        return "pong " + pingService.incrementPingsCount();
    }

    @ResponseBody
    @GetMapping("/count")
    public String count() {
        return String.valueOf(pingService.getPingsCount());
    }

}
