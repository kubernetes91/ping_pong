package org.example.ping_pong.services;

import lombok.RequiredArgsConstructor;
import org.example.ping_pong.dao.PingsDao;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Primary
@Service
@RequiredArgsConstructor
public class PersistentPingService implements PingService {

    private final PingsDao pingsDao;

    @Override
    public synchronized int incrementPingsCount() {
        return pingsDao.incrementPingsCount();
    }

    @Override
    public int getPingsCount() {
        return pingsDao.getPingsCount();
    }
}
