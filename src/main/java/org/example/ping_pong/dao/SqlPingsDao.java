package org.example.ping_pong.dao;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class SqlPingsDao implements PingsDao {

    private final JdbcTemplate jdbcTemplate;

    @Override
    public int getPingsCount() {
        String sql = """
                SELECT value FROM counters WHERE name = 'counter';
                """;
        Integer count = jdbcTemplate.queryForObject(sql, Integer.class);
        return count == null ? 0 : count;
    }

    @Override
    public int incrementPingsCount() {
        String sql = """
                UPDATE counters SET value = value + 1 WHERE name = 'counter';
                """;
        jdbcTemplate.execute(sql);
        return getPingsCount();
    }
}
