CREATE TABLE counters (
                       name TEXT NOT NULL,
                       value INTEGER NOT NULL,
                       unique(name)
);

INSERT INTO counters (name, value) VALUES ('counter', 0);